

MAGMA: inference of sparse microbial association networks
=========

MAGMA is a method for detecting interactions between microbiota that takes into account the noisy structure of the metagenomic count data, involving:
* an excess of zero counts,
* overdispersion of sequencing data,
* compositionality,
* possible covariate inclusion.

The method is based on Copula Gaussian graphical models whereby we model the marginals with zero inflated negative binomial generalized linear models.

The acronym MAGMA stands for **M**icrobial **A**ssociation **G**raphical **M**odel **A**nalysis.<br/>
MAGMA is implemented as an R package called rMAGMA.

## Installation of rMAGMA ##

To install the rMAGMA package from gitlab, enter the following commands in an R session.  


```r
library(devtools)
install_gitlab("arcgl/rmagma")
library(rMAGMA)
```

If you get an error, you may need to install a missing dependency.

##  Example of use: Analysis of the human stool microbiome from the HMP project ##

HMP dataset loading

```r
library(rMAGMA)
data("HMP")
```

Selection of stool microbiota

```r
otu_table_0 <- HMP$otu_table[HMP$row_metadata$bodysite== "Stool",]
metadata_0  <- HMP$row_metadata[HMP$row_metadata$bodysite== "Stool",]
taxo_0      <- HMP$col_taxonomy
```

Filter OTUs with prevalence <25% and samples with sequencing depth <500 reads

```r
prevalence       <- colMeans(otu_table_0>0)
sequencing_depth <- rowSums(otu_table_0)

icol <- prevalence >0.25
irow <- sequencing_depth >500

otu_table <- otu_table_0[irow,icol]
metadata  <- metadata_0[irow,]
taxo_Stool <- taxo_0[,icol]
```

Inference of the association network with MAGMA

```r
magma_Stool <- magma(data = otu_table)
```

Plot the network and color the nodes according to their phylum

```r
plot(magma_Stool, V.color.factor=taxo_Stool[2,])
```

![plot of chunk unnamed-chunk-6](figure/unnamed-chunk-6-1.png)




