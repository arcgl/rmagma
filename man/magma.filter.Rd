% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/filtering.R
\name{magma.filter}
\alias{magma.filter}
\title{Filtering function for metagenomic data}
\usage{
magma.filter(otu_table, min_prev = 0.1, min_seqdep = 100)
}
\arguments{
\item{otu_table}{data to filter}

\item{min_prev}{threshold on prevalence values of OTUs (rate of non-zero values of OTUs). OTUs with a prevalence below the threshold are discarded. 0.1 by default}

\item{min_seqdep}{threshold on sequencing depth of samples. Samples with sequencing depths below the threshold are removed. 100 by default}
}
\description{
Filtering function for metagenomic data
}
\examples{
data("bee")

raw_data <- bee$otu_table
dim(raw_data)

filtered_data1 <- magma.filter(otu_table=raw_data, min_prev = 0.05, min_seqdep = 50)
dim(filtered_data1)
filtered_data2 <- magma.filter(otu_table=raw_data, min_prev = 0.1, min_seqdep = 100)
dim(filtered_data2)
filtered_data3 <- magma.filter(otu_table=raw_data, min_prev = 0.5, min_seqdep = 1000)
dim(filtered_data3)

}
